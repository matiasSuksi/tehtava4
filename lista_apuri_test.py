import unittest
import lista_apuri

lista = [1,2,3,4,5,6,7,8,9,10]

class TestaaListaApuri(unittest.TestCase):
    def testaa_parilliset(self):
        testaus = lista_apuri.parillisten_maara(lista)
        self.assertEqual(testaus,5)

    def testaa_parittomat(self):
        testaus = lista_apuri.parittomien_maara(lista)
        self.assertEqual(testaus,5)

    def testaa_negatiiviset(self):
        testaus = lista_apuri.negatiivisten_maara(lista)
        self.assertEqual(testaus,0)
